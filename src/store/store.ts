import { configureStore } from '@reduxjs/toolkit';
import {
  TypedUseSelectorHook,
  useSelector as useReduxSelector,
} from 'react-redux';
import querySlice from './slices/querySlice';
import weatherSlice from './slices/weatherSlice';

const store = configureStore({
  reducer: {
    query: querySlice,
    weather: weatherSlice,
  },
});

type RootState = ReturnType<typeof store.getState>;

export default store;
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
