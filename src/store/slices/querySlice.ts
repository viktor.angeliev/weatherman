import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { APIQueryCity } from '../../api/API';
import { Place } from '../../api/Models';

interface WeatherState {
  places: Place[];
  loading: boolean;
  error: boolean;
}
const initialState = {
  places: [],
  loading: false,
  error: false,
} as WeatherState;

export const queryCity = createAsyncThunk<
  Place[],
  string,
  { rejectValue: void }
>('weather/queryCity', async (query, thunk) => {
  const response = await APIQueryCity(query);
  if (response.status === 'ok') {
    return response.body;
  } else {
    return thunk.rejectWithValue();
  }
});

const slice = createSlice({
  name: 'weather',
  initialState,
  reducers: {
    clearPlaces: (state) => {
      state.places = [];
    },
  },
  extraReducers: (builder) => {
    builder.addCase(queryCity.pending, (state) => {
      state.loading = true;
      state.error = false;
      state.places = [];
    });
    builder.addCase(queryCity.fulfilled, (state, action) => {
      state.loading = false;
      state.error = false;
      state.places = action.payload;
    });
    builder.addCase(queryCity.rejected, (state) => {
      state.loading = false;
      state.error = true;
      state.places = [];
    });
  },
});

export const { clearPlaces } = slice.actions;
export default slice.reducer;
