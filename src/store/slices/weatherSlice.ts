import { Place, Weather } from '../../api/Models';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import ThunkStatus from '../../utils/ThunkStatus';
import { APIGetWeather, APIQueryByID } from '../../api/API';

interface WeatherState {
  place: Place | null;
  weather: Weather | null;
  status: ThunkStatus;
}

const initialState = {
  place: null,
  weather: null,
  status: ThunkStatus.IDLE,
} as WeatherState;

export const queryWeatherByOsmID = createAsyncThunk<
  {
    place: Place;
    weather: Weather;
  },
  string,
  { rejectValue: void }
>('weather/queryByLonLat', async (osm_id, thunk) => {
  const placeResponse = await APIQueryByID(osm_id);
  if (placeResponse.status == 'error') return thunk.rejectWithValue();

  const place = placeResponse.body;
  const weatherResponse = await APIGetWeather(place.lat, place.lon);
  if (weatherResponse.status == 'error') return thunk.rejectWithValue();

  return { place: placeResponse.body, weather: weatherResponse.body };
});

const slice = createSlice({
  name: 'weather',
  initialState,
  reducers: {
    setPlace: (state, action: PayloadAction<Place>) => {
      state.place = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(queryWeatherByOsmID.pending, (state) => {
      state.status = ThunkStatus.LOADING;
      state.weather = null;
    });
    builder.addCase(queryWeatherByOsmID.fulfilled, (state, action) => {
      state.status = ThunkStatus.DONE;
      state.place = action.payload.place;
      state.weather = action.payload.weather;
    });
    builder.addCase(queryWeatherByOsmID.rejected, (state) => {
      state.status = ThunkStatus.ERROR;
      state.place = null;
      state.weather = null;
    });
  },
});

export default slice.reducer;
