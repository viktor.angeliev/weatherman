import { Place, SunsetSunrise, Weather } from './Models';

export interface BaseResponse {
  status: 'ok' | 'error';
  body?: unknown;
}

export interface ErrorResponse extends BaseResponse {
  status: 'error';
  body?: {
    error?: any;
  };
}

export interface WeatherResponse extends BaseResponse {
  status: 'ok';
  body: Weather;
}

export interface PlaceResponse extends BaseResponse {
  status: 'ok';
  body: Place;
}
export interface PlacesResponse extends BaseResponse {
  status: 'ok';
  body: Place[];
}

export interface SunsetSunriseResponse extends BaseResponse {
  status: 'ok';
  body: SunsetSunrise;
}
