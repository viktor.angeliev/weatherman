export interface Place {
  display_name: string;
  lat: string;
  lon: string;
  osm_id: string;
  osm_type: string;
  type: string;
  address: {
    country?: string;
    state?: string;
    municipality?: string;
    county?: string;
    city?: string;
    village?: string;
  };
}

export interface Weather {
  latitude: string;
  longitude: string;
  elevation: number;
  hourly: {
    temperature_2m: number[];
    time: string[];
  };
  daily: {
    time: string[]; //Dates
    sunrise: string[]; //Time
    sunset: string[]; //Time
    weathercode: number[];
    winddirection_10m_dominant: number[];
    temperature_2m_min: number[];
    temperature_2m_max: number[];
  };
  //hourly_units: {}.....
}

export interface SunsetSunrise {
  sunrise: string;
  sunset: string;
  /** Day length in seconds */
  day_length: string;
  //...other properties available
}
