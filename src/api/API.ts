import axios from 'axios';
import { Place, SunsetSunrise, Weather } from './Models';
import {
  ErrorResponse,
  PlaceResponse,
  PlacesResponse,
  SunsetSunriseResponse,
  WeatherResponse,
} from './Responses';

const geocodingURL =
  'https://nominatim.openstreetmap.org/search.php?format=jsonv2&addressdetails=1';

const reverseGeocodingURL =
  'https://nominatim.openstreetmap.org/lookup?format=jsonv2&addressdetails=1';

const weatherURL =
  'https://api.open-meteo.com/v1/forecast?daily=weathercode,temperature_2m_max,temperature_2m_min,sunrise,sunset,windspeed_10m_max,winddirection_10m_dominant&current_weather=true&timezone=Europe%2FMoscow&past_days=1';

const sunsetSunriseURL = 'https://api.sunrise-sunset.org/json?formatted=0';

export async function APIQueryCity(
  query: string
): Promise<PlacesResponse | ErrorResponse> {
  const response = await axios.get(geocodingURL, { params: { city: query } });
  if (response.status === 200) {
    const data: Place[] = response.data as Place[];
    const body = data.filter((place) =>
      ['city', 'village', 'administrative'].includes(place.type)
    );
    return {
      status: 'ok',
      body: body,
    };
  } else {
    return {
      status: 'error',
      body: {
        error: (response.data as any).error,
      },
    };
  }
}

export async function APIQueryByID(
  osm_id: string
): Promise<PlaceResponse | ErrorResponse> {
  const response = await axios.get(reverseGeocodingURL, {
    params: { osm_ids: osm_id },
  });
  const data = response.data as Place[];
  if (response.status === 200 && data.length > 0) {
    return {
      status: 'ok',
      body: data[0],
    };
  } else {
    return {
      status: 'error',
    };
  }
}

export async function APIGetWeather(
  latitude: string,
  longitude: string
): Promise<WeatherResponse | ErrorResponse> {
  const response = await axios.get(weatherURL, {
    params: { latitude, longitude },
  });
  if (response.status === 200) {
    return {
      status: 'ok',
      body: response.data as Weather,
    };
  } else {
    return {
      status: 'error',
      body: {
        error: (response.data as any).reason,
      },
    };
  }
}

export async function APIGetSunsetSunrise(
  latitude: number,
  longitude: number
): Promise<SunsetSunriseResponse | ErrorResponse> {
  const response = await axios.get(sunsetSunriseURL, {
    params: { lat: latitude, lng: longitude },
  });
  if (response.status === 200) {
    const data = response.data as any;
    return {
      status: 'ok',
      body: data.result as SunsetSunrise,
    };
  } else {
    return {
      status: 'error',
    };
  }
}
