import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#0CB88A',
    },
    secondary: {
      main: '#084B83',
    },
  },
});

export default theme;
