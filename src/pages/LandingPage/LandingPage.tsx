import { Box, Paper, Typography } from '@mui/material';
import React from 'react';
import QueryCity from '../../components/QueryCity/QueryCity';

export default function LandingPage(): JSX.Element {
  return (
    <div className="page">
      <Paper className="page-content">
        <div>
          <Typography component="h1" variant="h3" align="center">
            Welcome to{' '}
            <Typography
              color="primary"
              component="span"
              fontWeight="bold"
              variant="inherit"
            >
              WeatherMan
            </Typography>
          </Typography>
          <Typography component="h2" variant="h5" align="center">
            <Typography color="secondary" variant="inherit" component="span">
              Search
            </Typography>{' '}
            for your city:
          </Typography>
        </div>
        <Box width={500} maxWidth="100%">
          <QueryCity />
        </Box>
      </Paper>
    </div>
  );
}
