import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import { queryWeatherByOsmID } from '../../store/slices/weatherSlice';
import { Paper } from '@mui/material';
import Weather from '../../components/Weather/Weather';

export default function WeatherPage(): JSX.Element {
  const dispatch = useDispatch();

  const { osm_id } = useParams<{ osm_id: string }>();
  useEffect(() => {
    dispatch(queryWeatherByOsmID(osm_id));
  }, [dispatch, osm_id]);

  return (
    <div className="page">
      <Paper className="page-content">
        <Weather />
      </Paper>
    </div>
  );
}
