enum ThunkStatus {
  IDLE,
  LOADING,
  ERROR,
  DONE,
}

export default ThunkStatus;
