import { Place } from '../api/Models';

export function PlaceGetReadableAddress({ address }: Place): string {
  const orderedArr: (string | undefined)[] = [];
  //Order the address values
  orderedArr.push(address.village);
  orderedArr.push(address.city);
  orderedArr.push(address.county);
  orderedArr.push(address.municipality);
  orderedArr.push(address.state);
  orderedArr.push(address.country);
  //Remove duplicates
  const uniquerArr = orderedArr.filter(function (item, pos, self) {
    return item != undefined && self.indexOf(item) == pos;
  });
  return uniquerArr.join(', ');
}
