import {
  Box,
  CircularProgress,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  TextField,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { clearPlaces, queryCity } from '../../store/slices/querySlice';
import SearchIcon from '@mui/icons-material/Search';
import { useSelector } from '../../store/store';
import LocationCityIcon from '@mui/icons-material/LocationCity';
import { useHistory } from 'react-router';
import { Place } from '../../api/Models';

export default function QueryCity(): JSX.Element {
  const history = useHistory();
  const dispatch = useDispatch();
  const [query, setQuery] = useState('');
  const places = useSelector((state) => state.query.places);
  const error = useSelector((state) => state.query.error);
  const loading = useSelector((state) => state.query.loading);

  const onSubmitForm = (event: React.FormEvent) => {
    event.preventDefault();
    dispatch(queryCity(query));
  };
  const goToWeather = (place: Place) => {
    const id = place.osm_type.charAt(0).toUpperCase() + place.osm_id;
    dispatch(clearPlaces());
    history.push(`/${id}`);
  };
  return (
    <div>
      <form onSubmit={onSubmitForm}>
        <Box display="flex">
          <TextField
            value={query}
            onChange={(e) => setQuery(e.target.value)}
            label="Search"
            variant="outlined"
            color="secondary"
            size="small"
            fullWidth
          />
          <IconButton color="secondary" type="submit">
            <SearchIcon />
          </IconButton>
        </Box>
      </form>
      <Box display="flex" flexDirection="column" alignItems="center" pt={2}>
        {error && <Typography color="error">There was an error.</Typography>}
        {loading && <CircularProgress />}
        {places.length > 0 && (
          <List>
            {places.map((place) => (
              <ListItemButton
                key={place.osm_id}
                onClick={() => goToWeather(place)}
              >
                <ListItemIcon>
                  <LocationCityIcon />
                </ListItemIcon>
                <ListItemText primary={place.display_name} />
              </ListItemButton>
            ))}
          </List>
        )}
      </Box>
    </div>
  );
}
