import React from 'react';
import { Typography } from '@mui/material';

type Props = {
  angle: number;
};

export default function WindIcon({ angle }: Props): JSX.Element {
  return (
    <Typography color="secondary">
      <i className={`wi wi-wind towards-${angle}-deg wind-icon`} />
    </Typography>
  );
}
