import React from 'react';
import { Typography } from '@mui/material';

type Props = {
  code: number;
};

const CodeIconMap = new Map([
  [0, 'wi-day-sunny'],
  [1, 'wi-day-cloudy'],
  [2, 'wi-cloud'],
  [3, 'wi-cloudy'],
  [45, 'wi-day-fog'],
  [48, 'wi-day-fog'],
  [51, 'wi-day-sprinkle'],
  [53, 'wi-day-sprinkle'],
  [55, 'wi-day-storm-showers'],
  [56, 'wi-snowflake-cold'],
  [57, 'wi-snowflake-cold'],
  [61, 'wi-sprinkle'],
  [63, 'wi-rain'],
  [65, 'wi-hail'],
  [71, 'wi-snow'],
  [73, 'wi-snow'],
  [75, 'wi-snowflake-cold'],
  [77, 'wi-snow'],
  [80, 'wi-sprinkle'],
  [81, 'wi-rain'],
  [82, 'wi-rain'],
  [85, 'wi-snow'],
  [86, 'wi-snow'],
  [95, 'wi-thunderstorm'],
  [96, 'wi-thunderstorm'],
  [99, 'wi-tornado'],
]);

export default function WeatherIcon({ code }: Props): JSX.Element {
  return (
    <Typography color="secondary">
      <i className={`wi ${CodeIconMap.get(code)} weather-icon`} />
    </Typography>
  );
}
