import React from 'react';
import { useSelector } from '../../store/store';
import {
  Box,
  CircularProgress,
  Divider,
  Stack,
  Typography,
} from '@mui/material';
import ThunkStatus from '../../utils/ThunkStatus';
import { PlaceGetReadableAddress } from '../../utils/ModelUtils';
import './Weather.css';
import WeatherIcon from './WeatherIcon';
import WindIcon from './WindIcon';
import { Link } from 'react-router-dom';

export default function Weather(): JSX.Element {
  const status = useSelector((store) => store.weather.status);
  const place = useSelector((store) => store.weather.place);
  const weather = useSelector((store) => store.weather.weather);

  return (
    <>
      <Typography variant="h6" textAlign="left" width="100%">
        <Link to="/">Check another city</Link>
      </Typography>
      <Stack className="weather-stack" spacing={1} alignItems="center">
        {status == ThunkStatus.LOADING && <CircularProgress />}
        {status == ThunkStatus.ERROR && (
          <Typography color="error">
            There was an error loading weather data.
          </Typography>
        )}
        {place && weather && (
          <>
            <Typography
              component="h1"
              variant="h4"
              width="100%"
              textAlign="left"
            >
              Weather for
              <Typography variant="inherit" color="primary">
                {PlaceGetReadableAddress(place)}
              </Typography>
            </Typography>

            <Stack direction="row" className="weekdays-stack">
              {weather.daily.time.map((time, index) => {
                const date = new Date(time).toLocaleDateString('default', {
                  day: '2-digit',
                  month: 'short',
                });
                const weekDay = new Date(time).toLocaleDateString('default', {
                  weekday: 'long',
                });
                const code = weather.daily.weathercode[index];
                const angle = weather.daily.winddirection_10m_dominant[index];
                const tempMax = weather.daily.temperature_2m_max[index];
                const tempMin = weather.daily.temperature_2m_min[index];
                const sunrise = new Date(
                  weather.daily.sunrise[index]
                ).toLocaleTimeString('default', {
                  hour: '2-digit',
                  minute: '2-digit',
                  hourCycle: 'h23',
                });
                const sunset = new Date(
                  weather.daily.sunrise[index]
                ).toLocaleTimeString('default', {
                  hour: '2-digit',
                  minute: '2-digit',
                  hourCycle: 'h23',
                });

                return (
                  <>
                    <Stack
                      key={index}
                      className={`weekday-item ${index == 1 ? 'today' : ''}`}
                    >
                      <Box textAlign="center">
                        <Typography variant="h6">{weekDay}</Typography>
                        <Typography>{date}</Typography>
                      </Box>
                      <Divider />

                      <WeatherIcon code={code} />
                      <Box>
                        Max:
                        <Typography variant="h4">{tempMax} °C</Typography>
                        Мin:
                        <Typography variant="h4">{tempMin} °C</Typography>
                      </Box>
                      <Divider />

                      <Box>
                        Wind:
                        <WindIcon angle={angle} />
                        <Typography variant="h4" textAlign="center">
                          {angle}&nbsp;°
                        </Typography>
                      </Box>
                      <Divider />

                      <Typography>Sunrise: {sunrise}</Typography>
                      <Typography>Sunset: {sunset}</Typography>
                    </Stack>
                    <Divider orientation="vertical" />
                  </>
                );
              })}
            </Stack>
          </>
        )}
      </Stack>
    </>
  );
}
