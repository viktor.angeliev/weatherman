import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LandingPage from './pages/LandingPage/LandingPage';
import { Provider } from 'react-redux';
import store from './store/store';
import theme from './style/Theme';
import './style/global.css';
import './style/weather-icons/weather-icons.css';
import './style/weather-icons/weather-icons-wind.css';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { CssBaseline, ThemeProvider } from '@mui/material';
import WeatherPage from './pages/WeatherPage/WeatherPage';

function App(): JSX.Element {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route path="/:osm_id">
                <WeatherPage />
              </Route>
              <Route path="/">
                <LandingPage />
              </Route>
            </Switch>
          </BrowserRouter>
        </Provider>
      </ThemeProvider>
    </div>
  );
}

export default App;
